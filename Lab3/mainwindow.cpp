#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QString>
#include <QRegularExpression>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_released()
{
    QString name = ui->name->text();
    QString email = ui->email->text();
    QString subject = ui->subject->text();
    QString body = ui->body->toPlainText();

    QRegularExpression nameRegExp("^[a-z ,.'-]+$", QRegularExpression::CaseInsensitiveOption);
    QRegularExpression emailRegExp("^(.+)@(\\S+)$", QRegularExpression::CaseInsensitiveOption);

    if (!nameRegExp.match(name).hasMatch()) {
        QMessageBox::critical(this, "Bad name", "You have entered a bad name!");
        return;
    }

    if (!emailRegExp.match(email).hasMatch()) {
        QMessageBox::critical(this, "Bad email", "You entered an invalid email!");
        return;
    }

    if (subject.length() > 100) {
        QMessageBox::critical(this, "Bad subject", "Subject must be 100 characters or less!");
        return;
    }

    if (subject.length() > 10000) {
        QMessageBox::critical(this, "Bad message body", "Message body is too long");
        return;
    }

    if (subject.length() == 0 && body.length() == 0) {
        QMessageBox::critical(this, "Bad message contents", "Email must at least either have a subject or a body");
        return;
    }

    QMessageBox::information(this, "Validated successfully", "Everything you entered appears to be correct!");
}
